#!/usr/bin/env bash

# Remove existing npm resources
rm -rf /var/www/html/m150/quiz/node_modules
rm -rf /var/www/html/m150/quiz/package-lock.json
rm -rf /var/www/html/m150/quiz/yarn.lock

# Setup new directory outside of shared folder and link it into shared folder
mkdir /home/webadmin/m150
mkdir /home/webadmin/m150/node_modules
ln -s /home/webadmin/m150/node_modules /var/www/html/m150/quiz/node_modules

# Install node packages
cd /var/www/html/m150/quiz/
npm install