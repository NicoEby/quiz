<?php

use App\Quiz;
use App\User;
use App\UserPoints;
use App\League;
use Illuminate\Http\Request;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

/*
Route::middleware('auth:api')->get('/user', function (Request $request) {
    return $request->user();
});
*/

Route::post('signUp', 'Auth\AuthController@signUp');
Route::post('login', 'Auth\AuthController@login');
Route::middleware('auth:api')->get('/profile','Auth\AuthController@getUser');
Route::middleware('auth:api')->get('/logout','Auth\AuthController@logout');

Route::apiResources([
    'quizzes' => 'QuizController',
    'questions' => 'QuestionController',
    'answers' => 'AnswerController',
]);

Route::get('/quizzes/{quiz}/questions', 'QuestionController@indexForQuiz');
Route::get('/questions/{question}/answers', 'AnswerController@indexForQuestion');
Route::post('/quizzes/savePoints', 'QuizController@saveUserPoints');
Route::get('/scoreBoard', 'QuizController@getScoreBoard');
Route::get('/save', function () {
    return League::getCorrespondingLeague(User::find(24)->getPointsOfWeekForCurrentUser(new DateTime()));
});
