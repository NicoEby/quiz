<?php

namespace App\Http\Controllers;

use App\Quiz;
use App\UserPoints;
use App\League;
use App\User;
use Illuminate\Http\Request;

class QuizController extends Controller
{
    /**
     * QuizController constructor.
     */
    public function __construct()
    {
        $this->middleware('auth:api');
        $this->authorizeResource(Quiz::class, 'quiz');
    }


    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $quizzes = Quiz::all();

        return response($quizzes);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param \Illuminate\Http\Request $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $request->validate([
            'title' => 'required',
        ]);

        $quiz = new Quiz([
            'title' => $request->get('title'),
            'description' => $request->get('description'),
        ]);

        $quiz->save();

        return response($quiz, 201);
    }

    /**
     * Display the specified resource.
     *
     * @param \App\Quiz $quiz
     * @return \Illuminate\Http\Response
     */
    public function show(Quiz $quiz)
    {
        return response($quiz);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param \Illuminate\Http\Request $request
     * @param \App\Quiz $quiz
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Quiz $quiz)
    {
        $request->validate([
            'title' => 'required',
        ]);

        $quiz->fill($request->all());
        $quiz->save();

        return response(null, 204);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param \App\Quiz $quiz
     * @return \Illuminate\Http\Response
     */
    public function destroy(Quiz $quiz)
    {
        $quiz->delete();
        return response(null, 204);
    }

        /**
     * Store a newly created resource in storage.
     *
     * @param \Illuminate\Http\Request $request
     * @return \Illuminate\Http\Response
     */
    public function saveUserPoints(Request $request)
    {
        $request->validate([
            'points' => 'required',
            'userId' => 'required'
        ]);

        $up = new UserPoints([
            'points' => $request->get('points'),
            'user_id' => $request->get('userId')

        ]);

        $up->save();

        return response($up, 201);
    }

    public function getScoreboard(Request $request)
    {
        $request->validate([
            'userId' => 'required'
        ]);

        $league = League::getCorrespondingLeague(User::find($request->get('userId'))->getPointsOfWeekForCurrentUser(new \DateTime()));

        $players = User::getAllUsersOfLeague($league->id, new \DateTime());

        $reponse['name'] = $league->name;
        $reponse['players'] = $players;
        return $reponse;

    }

}
