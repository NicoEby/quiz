<?php

namespace App;

use Illuminate\Notifications\Notifiable;
use Illuminate\Contracts\Auth\MustVerifyEmail;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Illuminate\Support\Facades\DB;
use Laravel\Passport\HasApiTokens;
use App\UserPoints;
use App\League;

class User extends Authenticatable
{
    use HasApiTokens, Notifiable;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'name', 'email', 'password',
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'password', 'remember_token',
    ];

    /**
     * The attributes that should be cast to native types.
     *
     * @var array
     */
    protected $casts = [
        'email_verified_at' => 'datetime',
    ];

    public function quizzes()
    {
        return $this->hasMany(Quiz::class);
    }

    public static function getAllUsersOfLeague($leagueId, $date)
    {
        $league = League::find($leagueId);

        $leagueAbove = League::where('min_points', '>', $league->min_points)->orderBy('min_points')->first();

        if(!is_null($leagueAbove))
            $userPoints = User::getPointsOfWeek($date, $leagueAbove->min_points, $league->min_points);
        else
            $userPoints = User::getPointsOfWeek($date, null, $league->min_points);
        return $userPoints;
    }

    public function getPointsOfWeekForCurrentUser($date)
    {
        $week = $date->format("W");
        $year = $date->format("Y");
        $startDate = new \DateTime();
        $startDate->setISODate($year, $week);
        $endDate = clone $startDate;
        $endDate->modify('+6 days');

        $points = UserPoints::where('user_id', $this->id)->whereBetween('created_at', [$startDate, $endDate])->sum('points');
        return $points;
    }


    public static function getPointsOfWeek($date, $max, $min)
    {
        $week = $date->format("W");
        $year = $date->format("Y");
        $startDate = new \DateTime();
        $startDate->setISODate($year, $week);
        $endDate = clone $startDate;
        $endDate->modify('+7 days');

        /*$points = UserPoints::whereBetween('created_at', [$startDate, $endDate])->groupBy('user_id')
        ->selectRaw('sum(points) as sum, user_id')
        ->pluck('sum','user_id');*/

        $start = $startDate->format('Y-m-d');
        $end = $endDate->format('Y-m-d');

        $query = "select row_number() over(order by total desc) as place, (select name from users where id = user_id) as user_name, user_id, sum(points) as total from userpoints where created_at between DATE('$start') and date('$end') group by user_id having total > $min";

        if(!is_null($max)){
            $query = $query . " and total < $max ";
        }
        $query = $query . "order by place";

        $result = DB::select($query);
        return $result;
    }


}
