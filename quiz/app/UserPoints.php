<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class UserPoints extends Model
{
    protected $fillable = [
        'points',
        'user_id'
    ];

    protected $table = 'userpoints';

    public function owner()
    {
        return $this->belongsTo(User::class);
    }

}
