<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class League extends Model
{
    protected $table = 'league';
    public static function getCorrespondingLeague($points)
    {
        return League::where('min_points', '<=', $points)->orderBy('min_points', 'desc')->first();
    }
}
